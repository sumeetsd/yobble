# Yobble

<h3>** Don't forget what you want to do, just get them done on time **</h3>

<p>A simple todo app to manage your daily tasks. You can also set reminders and sync your data on the cloud. This is a <b>cross platform</b> app, which means you can use it on your favourite operating system, let it be <b>Windows</b> or <b>Mac</b> or <b>Linux</b>.</p>

<p>This application is built with <b>Node.js</b> and its frontend is powered by <b>electron</b> a framework on which <b>Atom</b> is built.</p>

<h2>Setup</h2>

<p>Install NodeJS v6.0 or above from nodejs.org</p>
<pre>
$ git clone https://github.com/sumeetsd700/yobble.git
$ cd yobble
$ npm install
</pre>

<h2>Usage</h2>
<p>Simple run it with the electron</p>

<h2>Found Bugs</h2>
<p>Feel free to report bugs to me at <b><i>dewangan.sumeet700@gmail.com</i></b>.</p>

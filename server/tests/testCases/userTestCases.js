// Library Modules for testing
const expect = require('expect');
const request = require('supertest');
const {ObjectID} = require('mongodb');

// Custom modules
const {app} = require('./../../server');
const {User} = require('./../../models/user');
const {user_array} = require('./../seed/seed');

var testUser = {
    "name": "A Test User",
    "user_name": "testUser",
    "email": "user@test.com",
    "password": "Test@123"
}

var invalidUser = {
    "name": "Invalid Test User",
    "user_name": "ok",
    "email": "invaliduser@test.com",
    "password": "Test@123"
}

var copyUser = {
    "name": "Copy Test User",
    "user_name": "testUser",
    "email": "copyuser@test.com",
    "password": "Test@123"
}

var addUser = () => {
    it('should create a user', (done) => {
        request(app)
        .post('/users')
        .send(testUser)
        .expect(200)
        // .expect((res) => {
        //     expect(res.headers['x-auth']).toExist();
        //     expect(res.body.user_name).toBe(testUser.user_name);
        //     expect(res.body._id).toExist();
        // })
        .end(done);
    });

    it('should return validation error for invalid data', (done) => {
        request(app)
        .post('/users')
        .send(invalidUser)
        .expect(400)
        .expect((res) => {
            expect(res.body).toEqual({});
        })
        .end(done);
    });

    // it('should not create a user if its username / email exists', (done) => {
    //     request(app)
    //     .post('/users')
    //     .send(testUser)
    //     .expect(400)
    //     .expect((res) => {
    //         expect(res.body.errmsg).toExist();
    //     })
    //     .end(done);
    // });
}

var getUserInfo = () => {
    it('should return user if authenticated', (done) => {
        request(app)
        .get('/users/dashboard')
        .set('x-auth',user_array[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
            expect(res.body.user_name).toBe(user_array[0].user_name);
            expect(res.body._id).toBe(user_array[0]._id.toHexString());
        })
        .end(done);
    });

    it('should return a http 401 status code if not authenticated', (done) => {
        request(app)
        .get('/users/dashboard')
        .expect(401)
        .expect((res) => {
            expect(res.body).toEqual({});
        }).end(done);
    });
};

var login = () => {
    it('should login a user with vaid email and password and return an x-auth token', (done) => {
        request(app)
        .post('/users/login')
        .send({email: user_array[1].email, password: user_array[1].password})
        .expect(200)
        .expect((res) => {
            expect(res.header['x-auth']).toExist();
        }).end(done);
    });

    it('should return 400 for a valid email but incorrect password', (done) => {
        request(app)
        .post('/users/login')
        .send({email: "user1@test.com", password: "TestUser2"})
        .expect(400)
        .end(done);
    });

    it('should return 400 for an invalid email', (done) => {
        request(app)
        .post('/users/login')
        .send({email: "test@test.com", password: "TestUser1"})
        .expect(400)
        .end(done);
    });
};

var logoutUserWithToken = () => {
    it('should remove auth token on logout', (done) => {
        request(app)
        .delete('/users/dashboard')
        .set('x-auth', user_array[1].tokens[0].token)
        .expect(200)
        .end((err, res) => {
            if(err) {
                return done(err);
            }
            User.findById(user_array[1]._id).then((user) => {
                expect(user.tokens.length).toBe(0);
                done();
            }).catch((err) => {
                done(err);
            });
        });
    });
};

module.exports = {getUserInfo, addUser, login, logoutUserWithToken};

// Library Modules for testing
const expect = require('expect');
const request = require('supertest');
const {ObjectID} = require('mongodb');

// Custom modules
const {app} = require('./../../server');
const {Todo} = require('./../../models/todo');
const {todo_array} = require('./../seed/seed');

var addTodos = () => {
    // Sending a valid test string to validate data insertion
    it('should create a new todo', (done) => {
        var text = 'Test String';

        request(app).post('/todos').send({
            text
        }).expect(200).expect((res) => {
            expect(res.body.text).toBe(text);
        }).end((err, res) => {
            if (err) {
                return done(err);
            }
            Todo.find({text}).then((todos) => {
                expect(todos.length).toBe(1);
                expect(todos[0].text).toBe(text);
                done();
            }).catch((err) => {
                done(err);
            });
        });
    });
    // Sending an invalid test string, to validate that invalid data does not gets inserted
    it('should not create a todo with invalid body', (done) => {
        request(app).post('/todos').send({}).expect(400).end((err, res) => {
            if(err) {
                return done(err);
            }
            Todo.find().then((todos) => {
                expect(todos.length).toBe(3);
                done();
            }).catch((err) => {
                done(err);
            });
        });
    });
};

var getTodos = () => {
    it('should get all todos', (done) => {
        request(app).get('/todos').expect(200).expect((res) => {
            expect(res.body.todos.length).toBe(3);
        }).end(done);
    });
};

var getTodoWithID = () => {
    // Checking if it returns a todo document for a valid id
    it('should return a todo document according to the given id', (done) => {
        request(app).get(`/todos/${todo_array[1]._id.toHexString()}`).expect(200).expect((res) => {
            expect(res.body.item.text).toBe(todo_array[1].text);
        }).end(done);
    });

    // Checking if it returns a 404 status code for an invalid todo id
    it('should return a 404, i.e. an invalid todo id', (done) => {
        request(app).get('/todos/123').expect(404).end(done);
    });

    // Checking if it returns a 400 status code if a todo id is valid but not found
    it('should return 400, i.e. todo with given id is not found', (done) => {
        request(app).get('/todos/597b0ef420fabc55f91d1b00').expect(400).end(done);
    });
};

var updateTodoWithID = () => {
    it('should not update a todo with invalid todo_id', (done) => {
        request(app).patch('/todos/123').expect(404).end(done);
    });

    it('should not update a todo with a valid id, if it does not exists', (done) => {
        request(app).patch('/todos/597b05ed316513e64022020c').expect(404).end(done);
    });

    it('should update a todo to completed = true with the given todo_id', (done) => {
        request(app).patch(`/todos/${todo_array[0]._id.toHexString()}`).send({
            completed: true
        }).expect(200).expect((res) => {
            expect(res.body.item.completed).toBe(true);
            expect(res.body.item.completedAt).toBeA('number');
        }).end(done);
    });

    it('should update a todo to completed = false with the given todo_id', (done) => {
        request(app).patch(`/todos/${todo_array[2]._id.toHexString()}`).send({
            completed: false
        }).expect(200).expect((res) => {
            expect(res.body.item.completed).toBe(false);
            expect(res.body.item.completedAt).toBe(null);
        }).end(done);
    });
};

var deleteTodoWithID = () => {
    it('should not delete a todo with an invalid id', (done) => {
        request(app).get('/todos/123').expect(404).end(done);
    });

    it('should not delete a todo with a valid id, but its not present in the database', (done) => {
        request(app).get('/todos/597b0ef420fabc55f91d1aff').expect(400).end(done);
    });

    it('should delete a document with a given id', (done) => {
        request(app).get(`/todos/${todo_array[2]._id.toHexString()}`).expect(200).expect((res) => {
            expect(res.body.item.text).toBe(todo_array[2].text);
        }).end(done);
    });
};

var deleteAllTodos = () => {
    it('should delete all todos', (done) => {
        request(app).get('/todos').expect(200).end(done);
    });
};

module.exports = {addTodos, getTodos, getTodoWithID, updateTodoWithID, deleteTodoWithID, deleteAllTodos};

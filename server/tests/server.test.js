// Custom modules
const todoCases = require('./testCases/todoTestCases');
const userCases = require('./testCases/userTestCases');
const {populateTodos, populateUsers} = require('./seed/seed');

// Removing existing todo from database and inserting the sample dataset - for each test case
beforeEach(populateUsers);
beforeEach(populateTodos);

// Testing POST request for data insertion
describe('POST /todos', todoCases.addTodos);

// GET request to fetch all the todos
describe('GET /todos', todoCases.getTodos);

// GET request to fetch individual todos
describe('GET /todos/:todo_id', todoCases.getTodoWithID);

// PATCH request to update document
describe('PATCH /todos/:todo_id', todoCases.updateTodoWithID);

// DELETE a document with a given todo_id
describe('DELETE /todos/:todo_id', todoCases.deleteTodoWithID);

// DELETE all documents
describe('DELETE /todos', todoCases.deleteAllTodos);

// Add a User
describe('Post /users', userCases.addUser);

// Get User Info with a user username
describe('GET /users/dashboard', userCases.getUserInfo);

// Login a user
describe('POST /users/login', userCases.login);

// Logout A User
describe('DELETE /users/dashboard', userCases.logoutUserWithToken);

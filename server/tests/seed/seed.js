const {ObjectID} = require('mongodb');
const {Todo} = require('./../../models/todo');
const {User} = require('./../../models/user');
const jwt = require('jsonwebtoken');

const userOneID = new ObjectID();
const userTwoID = new ObjectID();

const user_array = [{
    _id: userOneID,
    name: "Test User 1",
    user_name: "testUser1",
    email: "user1@test.com",
    password: "TestUser1",
    tokens: [{
        access: "auth",
        token: jwt.sign({_id: userOneID, access: "auth"}, 'salt').toString()
    }]
}, {
    _id: userTwoID,
    name: "Test User 2",
    user_name: "testUser2",
    email: "user2@test.com",
    password: "TestUser2"
}];

// Sample dataset
const todo_array = [{
    _id: ObjectID(),
    _creator: userOneID,
    text: "First Todo Item"
}, {
    _id: ObjectID(),
    _creator: userTwoID,
    text: "Second Todo Item"
}, {
    _id: ObjectID(),
    _creator: userOneID,
    text: "Third Todo Item",
    completed: true,
    completedAt: new Date().getTime()
}];

// Clear and then populate Todos
var populateTodos = (done) => {
    Todo.remove({}).then(() => {
        return Todo.insertMany(todo_array);
    }).then(() => {
        done();
    });
};

// Clear and then popualate Users
var populateUsers = (done) => {
    User.remove({}).then(() => {
        var userOne = new User(user_array[0]);
        var userTwo = new User(user_array[1]);

        userOne.save();
        userTwo.save();

        return Promise.all([userOne, userTwo]);
    }).then(() => {
        done();
    });
};

module.exports = {user_array, todo_array, populateTodos, populateUsers};

// Library modules
const {ObjectID} = require('mongodb');
const _ = require('lodash');

// Custom modules
var {User} = require("./../models/user");

// Creates and store a token for user while signing up
var addUserWithToken = (req, res) => {
    var body = _.pick(req.body, ['name', 'user_name', 'email', 'password']);
    var user = new User(body);

    user.save().then(() => {
        // returning it for a chaining promise
        return user.generateAuthToken();
    }).then((token) => {
        res.header('x-auth', token).status(200).send(user);
    }).catch((err) => {
        var err_msg = _.pick(err, ['errmsg']);
        res.status(400).send(err_msg);
    });
};

var addUser = (req, res) => {
    var body = _.pick(req.body, ['name', 'user_name', 'email', 'password']);
    var user = new User(body);

    user.save().then(() => {
        return res.status(200).send({email: user.email, user_name: user.user_name});
    }, (err) => {
        return res.status(400).send();
    });
};

var fetchUserWithTokenID = (req, res) => {
    res.status(200).send(req.user);
};

var userLogin = (req, res) => {
    // user identifier to identify user by email or by user name
    var body = _.pick(req.body, ['email', 'password']);

    User.findByCredentials(body.email, body.password).then((user) => {
        return user.generateAuthToken().then((token) => {
            res.header('x-auth', token).status(200).send();
        }).catch((err) => {
            res.status(400).send();
        });
        res.status(200).send(user);
    }).catch((err) => {
        res.status(400).send();
    });
};

var logoutUserWithToken = (req, res) => {
    User.removeToken(req.headers['x-auth']).then((result) => {
        console.log(result);
        res.status(200).send();
    }, (err) => {
        res.status(400).send();
    });
};

module.exports = {addUser, fetchUserWithTokenID, userLogin, logoutUserWithToken};

// Library modules
const {ObjectID} = require('mongodb');
const _ = require('lodash');

// Custom modules
var {Todo} = require("./../models/todo");

var insertTodo = (req, res) => {
    var item = new Todo({
        text: req.body.text,
        _creator: req.user._id
    });

    item.save().then((result) => {
        res.send(result);
    }, (err) => {
        res.status(400).send(err);
    });
};

var fetchTodos = (req, res) => {
    Todo.find({
        _creator: req.user._id
    }).then((item) => {
        res.send({
            todos: item
        });
    }, (err) => {
        res.status(400).send(err);
    });
};

var testRequest = (req, item) => {
    if(!item) {
        return res.status(400).send();
    }
    if(JSON.stringify(item._creator) !== JSON.stringify(req.user._id)) {
        return res.status(401).send();
    }
};

var fetchTodoWithID = (req, res) => {
    var todo_id = req.params.todo_id;

    // Check if the given todo_id is a valid ObjectID
    if(!ObjectID.isValid(todo_id)) {
        return res.status(404).send();
    }

    Todo.findById(todo_id).then((item) => {
        testRequest(req, item);
        res.status(200).send({item});
    }).catch((err) => {
        return res.status(400).send();
    });
};

var deleteAllTodos = (req, res) => {
    Todo.remove({_creator: req.user._id}).then((todos) => {
        res.status(200).send(todos);
    }).catch((err) => {
        res.status(400).send();
    });
};

var deleteTodoWithID = (req, res) => {
    var todo_id = req.params.todo_id;

    if(!ObjectID.isValid(todo_id)) {
        return res.status(404).send();
    }

    Todo.findById(todo_id).then((item) => {
        testRequest(req, item);

        Todo.findByIdAndRemove(todo_id).then((item) => {
            res.status(200).send(item);
        }).catch((err) => {
            return res.status(400).send();
        });
    }).catch((err) => {
        return res.status(400).send();
    });
};

var updateTodo = (req, res) => {
    var todo_id = req.params.todo_id;
    var body = _.pick(req.body, ['text', 'completed']);

    if(!ObjectID.isValid(todo_id)) {
        return res.status(404).send();
    }

    if(_.isBoolean(body.completed) && body.completed) {
        body.completedAt = new Date().getTime();
    } else {
        body.completed = false;
        body.completedAt = null;
    }

    Todo.findById(todo_id).then((item) => {
        testRequest(req, item);

        Todo.findByIdAndUpdate(todo_id, {$set: body}, {new: true}).then((item) => {
            res.status(200).send({item});
        }).catch((err) => {
            res.status(400).send();
        });
    }).catch((err) => {
        return res.status(400).send();
    });
};

module.exports = {insertTodo, fetchTodos, fetchTodoWithID, deleteAllTodos, deleteTodoWithID, updateTodo};

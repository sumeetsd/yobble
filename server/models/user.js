const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require("bcryptjs");

var userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        minlength: 3
    },
    user_name: {
        type: String,
        required: true,
        trim: true,
        minlength: 4,
        unique: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        minlength: 10,
        unique: true,
        validate: {
            validator: validator.isEmail,
            message: "{VALUE} is not a valid email"
        }
    },
    password: {
        type: String,
        required: true,
        minlength: 6
    },
    tokens: [{
        access: {
            type: String,
            required: true
        },
        token: {
            type: String,
            required: true
        }
    }]
});

userSchema.pre('save', function(next) {
    var user = this;

    if(user.isModified('password')) {
        bcrypt.genSalt(10, (err, salt) => {
            if(err) {
                next();
            }
            bcrypt.hash(user.password, salt, (err, hash) => {
                if(err) {
                    next();
                }
                user.password = hash;
                next();
            });
        });
    } else {
        next();
    }
});

userSchema.methods.toJSON = function() {
    var user = this;
    var userObj = user.toObject();

    return _.pick(userObj, ['_id', 'user_name']);
};

userSchema.statics.findByToken = function(token) {
    var User = this;
    var decoded;

    try {
        decoded = jwt.verify(token, "salt");
    } catch(err) {
        // return new Promise((resolve, reject) => {
        //     reject();
        // });
        return Promise.reject();
    }

    return User.findOne({
        _id: decoded._id,
        'tokens.token': token,
        'tokens.access': 'auth'
    });
};

// here we work on each user instance
// we need this keyword to refer each document and the arrow function do not provide such functionality
userSchema.methods.generateAuthToken = function() {
    var user = this;

    var access = "auth";
    var token = jwt.sign({_id:user._id.toHexString(), access}, 'salt').toString();

    user.tokens.push({access, token});

    // we have to return token to another then call as paramerter in the server.js file.
    // so for returning it, we return the token in promise and for the promise to return some value to the next then call
    // we return the promise
    return user.save().then(() => {
        return token;
    });
};

userSchema.statics.findByCredentials = function(email, password) {
    var User = this;
    return User.findOne({email}).then((user) => {
        if(!user) {
            return Promise.reject();
        }
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, user.password, (err, result) => {
                if(result) {
                    resolve(user);
                }
                reject();
            });
        });
    });
};

userSchema.statics.removeToken = function(token) {
    var user = this;
    return user.update({
        $pull: {
            tokens: {
                token: token
            }
        }
    });
};

// Defining User Model
var User = mongoose.model('User', userSchema);

module.exports = {User};

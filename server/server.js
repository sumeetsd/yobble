// config module
require('./config/config');

// Library Modules
const express = require('express');
const bodyParser = require('body-parser');

// Custom modules
var {mongoose} = require("./db/mongoose");
var {authenticate} = require("./middlewares/authenticate");
var todoRoutes = require('./routeDefinitions/todoRouteDefinitions');
var userRoutes = require('./routeDefinitions/userRouteDefinitions');

// Initializing Express App
var app = express();

// Defining middlewares
app.use(bodyParser.json());

// Defining Routes
// The POST route for inserting data
app.post('/todos', authenticate, todoRoutes.insertTodo);

// Fetch all the todos
app.get('/todos', authenticate, todoRoutes.fetchTodos);

// Fetch todo with a given id from get parameters
app.get('/todos/:todo_id', authenticate, todoRoutes.fetchTodoWithID);

// DELETE all todos
app.delete('/todos', authenticate, todoRoutes.deleteAllTodos);

// DELETE a todo with a given ID
app.delete('/todos/:todo_id', authenticate, todoRoutes.deleteTodoWithID);

// UPDATE (PATCH) a todo with given todoID
app.patch('/todos/:todo_id', authenticate, todoRoutes.updateTodo);

// Add user using POST
app.post('/users', userRoutes.addUser);

// Fetch user data with a token id
app.get('/users/dashboard', authenticate, userRoutes.fetchUserWithTokenID);

// Post User Login
app.post('/users/login', userRoutes.userLogin);

// Logout a user by deleting its access token
app.delete('/users/dashboard', authenticate, userRoutes.logoutUserWithToken);

// Setting up the server
app.listen(process.env.PORT, () => {
    console.log("Started on port 3000");
});

module.exports = {
    app
};

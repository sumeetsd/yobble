const {User} = require('./../models/user');

var authenticate = (req, res, next) => {
    var token = req.header('x-auth');

    // working on a model - so User, not user
    // if we work on an instance user should be used
    User.findByToken(token).then((user) => {
        if(!user) {
            return res.status(401).send();
        }
        req.user = user;
        req.token = token;
        next();
    }).catch((err) => {
        res.status(401).send();
    });
}

module.exports = {authenticate};

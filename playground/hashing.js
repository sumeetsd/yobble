const {SHA256} = require('crypto-js');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

// Tsseting simple Hashing
var message = "Hello World";

var hash = SHA256(message).toString();
console.log("message = \"" + message + "\"");
console.log("hashed string = \n" + hash + "\n");

// token generation
var data = {
    id: 11401222
};
var token = {
    data,
    hash: SHA256(JSON.stringify(data) + "someSaltText").toString()
}

// Modifying the data sent to the server
token.data.id = 100
// We would never know the salt text
token.hash = SHA256(JSON.stringify(data)).toString();

// Reciever Part
var resultHash = SHA256(JSON.stringify(data) + "someSaltText").toString();
if(resultHash === token.hash) {
    console.log("Actual Data");
} else {
    console.log("Data was changed");
}

// Validating using JWT

console.log("\nUsing JWT");

// signs and generates a token
new_data = {
    id: 12121
};

var new_token = jwt.sign(new_data, "saltText")

console.log("Genetated token");
console.log(new_token);

// Decodes a token
var decoded = jwt.verify(new_token, "saltText");
console.log("\nDecoded data = ");
console.log(decoded);

// Using bcrypt to hash passwords

console.log("\nUsing BCRYPT");

var pwd = "helloWorld";

bcrypt.genSalt(10, (err, salt) => {
    if(err) {
        return console.log("Error generating salt: " + salt);
    }
    console.log("Generated Salt = ");
    console.log(salt);

    bcrypt.hash(pwd, salt, (err, hash) => {
        if(err) {
            return console.log("Error generating hash");
        }
        console.log("Generated hash");
        console.log(hash);

        console.log("\nComparing passwords");

        pwd = 'helloworld';
        var hashedPwd = "$2a$10$avmRWggKBqxwwfFFBkE7AO0gtk9Cs9777wxwS9mfPo1wI5GpIgvAO";
        bcrypt.compare(pwd, hashedPwd, (err, result) => {
            if(err) {
                return console.log("Error in comparing passwords: " + err);
            }
            console.log(result);
        });
    });
});


console.log("\n");

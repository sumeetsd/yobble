// const mongoClient = require('mongodb').MongoClient;
const {
    MongoClient, ObjectID
} = require('mongodb');

var obj = new ObjectID();
console.log(obj);

MongoClient.connect("mongodb://localhost:27017/TodoApp", (err, db) => {
    if (err) {
        return console.log("Unable to connect to the database server");
    }
    console.log("Connected to mongodb server");

    db.collection("Todos").insertOne({
        text: "Something to do",
        completed: false
    }, (err, result) => {
        if (err) {
            return console.log("Unable to perfrom insertOne");
        }
        console.log(JSON.stringify(result.ops));
    });

    db.collection("Users").insertOne({
        name: "Sumeet Dewangan",
        age: 20,
        graduate: false
    }, (err, result) => {
        if (err) {
            return console.log("Cannot insert to database");
        }
        console.log(JSON.stringify(result.ops));
    });


    db.close();
});

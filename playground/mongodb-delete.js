const {
    MongoClient, ObjectID
} = require('mongodb');

MongoClient.connect("mongodb://localhost:27017/TodoApp", (err, db) => {
    if (err) {
        return console.log("Unable to connect to the database");
    }
    console.log("Todo: Successfully connected to db");

    // Delete Many
    // db.collection("Todos").deleteMany({
    //     completed: false
    // }).then((result) => {
    //     console.log(result);
    // }, (err) => {
    //     console.log("Error Deleting objects");
    // });

    // Delete One
    // db.collection("Todos").deleteOne({
    //     completed: false
    // }).then((result) => {
    //     console.log(result);
    // }, (err) => {
    //     console.log("Error Deleting A Document");
    // });

    // Find One and Delete
    db.collection("Todos").findOneAndDelete({
        completed: true
    }).then((result) => {
        console.log("Deleted " + JSON.stringify(result));
    }, (err) => {
        console.log(
            "Error encounterd while performing find one and delelte: " +
            err);
    })

    // db.close();
});

const {
    MongoClient, ObjectID
} = require('mongodb');

MongoClient.connect("mongodb://localhost:27017/TodoApp", (err, db) => {
    if (err) {
        return console.log("Error connecting to database server");
    }
    console.log("Connected to the database server");

    db.collection("Todos").find({
        completed: false
    }).toArray().then((docs) => {
        console.log("Todos: Success in promise");
        console.log(JSON.stringify(docs, undefined, 2));
    }, (err) => {
        console.log("Unable to fetch the Todos " + err);
    });

    db.collection("Users").find({
        _id: new ObjectID('59784fe8ba3d0345.2dacea01a')
    }).toArray().then((docs) => {
        console.log("Users: Success in promise");
        console.log(JSON.stringify(docs, undefined, 2));
    }, (err) => {
        console.log("Unable to fetch the Users " + err);
    });

    db.close();
});

const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect("mongodb://localhost:27017/TodoApp", (err, db) => {
    if(err) {
        return console.log("Cannot connect to database server");
    }

    console.log("Successfully connected to database server");
    db.collection("Todos").findOneAndUpdate({
        _id: new ObjectID('5978965221765276c619543b')
    }, {
        $set: {
            completed: true
        }
    }, {
        returnOriginal: false
    }).then((result) => {
        console.log(result);
    }, (err) => {
        console.log("Error occured while performing update operation");
    });

    db.close();
});
